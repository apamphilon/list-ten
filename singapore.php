<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Singapore travel tips! – List Ten</title>
    <meta name="description" content="List Ten is a simple travel idea... Check off the ten things on our list and you’ll go home happy.">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <!-- open graph -->
    <meta property="og:image" content="/img/opengraph.png" />

    <link rel="stylesheet" href="style.css">
    <script src="js/vendor/modernizr.min.js"></script>

    <!-- particles.js -->
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

    <!-- typekit -->
    <script src="https://use.typekit.net/hda8wft.js"></script>
    <script>try{Typekit.load({ async: false });}catch(e){}</script>

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/manifest.json">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body class="page-list nav-ref-singapore">
    <!--[if lt IE 9]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include_once('template-parts/color-pattern.php'); ?>
    <?php include_once('template-parts/header.php'); ?>
    <?php include_once('template-parts/overlay-nav.php'); ?>

    <div class="list-top">
      <div id="particles-js" style="position:absolute; top: 0; left: 0; width: 100%;"></div>
    </div>

    <div class="site-wrapper" id="js-site-wrapper">
      <?php include_once('template-parts/list-block-singapore.php'); ?>
      <?php include_once('template-parts/contact-block.php'); ?>
    </div><!-- #site-wrapper -->

    <?php include_once('template-parts/footer.php'); ?>
