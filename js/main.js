// define global vars
var $window = $(window);
var $document = $(document);
var $body = $('body');
var $siteWrapper = $('#js-site-wrapper');

// debounce function
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

$document.ready(function() {

  // function to detect if an element is in view
  $.fn.isOnScreen = function() {
    var viewport = {
      top : $window.scrollTop(),
      left : $window.scrollLeft()
    };
    viewport.right = viewport.left + $window.width();
    viewport.bottom = viewport.top + $window.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
  };

  // play / pause video when in / out of view
  var playPauseVid = debounce(function() {
    $('video').each(function() {
      if ( $(this).isOnScreen() ) {
        $(this)[0].play();
        // console.log('video is playing');
      } else {
        $(this)[0].pause();
        // console.log('video is paused');
      }
    })
  }, 250);

  $window.on('scroll', playPauseVid);

  // list complete alert
  function listComplete() {
    var $cityTitle = $('#js-city-block-wrapper .city-block__title').text();

    if ($('#js-list-block-wrapper input:checkbox:not(:checked)').length == 0) {
      // all are checked
      // Get the modal
      var modal = document.getElementById('myModal');

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName('close')[0];

      // add class to body (this opens the modal)
      $body.addClass('list-complete');

      // When the user clicks on button, close the modal
      span.onclick = function() {
        $body.removeClass('list-complete');
      }

      // When the user clicks on button, close the modal
      span.onclick = function() {
        $body.removeClass('list-complete');
      }

      // when the user hits esc, close the modal
      $document.keydown(function(e) {
        if (e.which == 27) {
          $body.removeClass('list-complete');
        }
      });
    }
  }

  $document.on('change', '#js-list-block-wrapper input:checkbox', function() {
    listComplete();
  });

  // list-block accordion
  $siteWrapper.on('click', '.list-block__title', function() {
    $('.list-block__description').slideUp().removeClass('opened');
  	$('.list-block__title').removeClass('active');
  	$(this).next('.list-block__description:hidden').slideDown().addClass('opened').prev('.list-block__title').addClass('active');
  });

  // about footer
  $('#js-about-trigger').on('click', function(e) {
    e.preventDefault();
    var $target = $('#js-about');
    $target.slideToggle(500);
    $('html,body').animate({
      scrollTop: $target.offset().top
    }, 500);
  });

  // particle.js
  function particles() {
    var $particles = $('#particles-js');
    /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
    if ($particles.length) {
      particlesJS.load('particles-js', 'particles.json', function() {
      });
    }
  }

  if ( $window.width() > 767 ) {
    particles(); //init
  }

  // pagination for list blocks
  // arrow key functionality
  $document.keydown(function(e) {
    switch(e.which) {
      case 37: // left
      break;

      case 39: // right
      break;

      default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)

    setTimeout(function() {
      if (e.which === 37 && !$body.hasClass('list-complete')) {
        $('.pagination__prev').click();
      } else if (e.which === 39 && !$body.hasClass('list-complete')) {
        $('.pagination__next').click();
      }
    });
  });

  // pagination click functionality
  $siteWrapper.on('click', '.pagination a', function(e) {
    // prevent default behaviour
    e.preventDefault();

    // functions
    function loadList() {
      $('#js-list-block-wrapper').load(destination + ' [id*="js-list-location-"]', function() {
        checkboxState(); // reload checkbox state
      });
    }

    function swapImage() {
      $('#js-city-block-wrapper').load(destination + ' #js-city-block');
    }

    function swapListMeta() {
      $('#js-list-meta-wrapper').load(destination + ' #js-list-meta');
    }

    function swapListComplete() {
      $('#js-list-complete-wrapper').load(destination + ' #js-list-complete');
    }

    function setPagination() {
      $('#js-pagination-wrapper').load(destination + ' #js-pagination');
    }

    // if current page stop script
    if (window.location === this.href) {
      return;
    }

    // get link destination
    var destination = $(this).attr('href');
    var destinationName = $(this).data('name');

    // update body class
      var bodyClass = 'nav-ref-' + destinationName.toLowerCase();

      // remove space from new-york
      bodyClass = bodyClass.replace('new york', 'new-york');

      $('body').removeClass('nav-ref-manchester nav-ref-new-york nav-ref-glasgow nav-ref-berlin nav-ref-austin nav-ref-singapore').addClass(bodyClass);

    // update url
    window.history.pushState(null, null, destination);

    // update page title
    document.title = destinationName + ' | List-ten';

    // load the list
    loadList();

    // swap the city flag image
    swapImage();

    // swap the list meta
    swapListMeta();

    // swap the list complete message
    swapListComplete();

    // update the pagination links
    setPagination();
  });

  $(function() {
    //Enable swiping...
    $('#js-list-block').swipe( {
      // exclue elements that need to be clickable
      excludedElements: '.list-block__title, input, label, .list-block__map-marker',

      //Generic swipe handler for all directions
      swipeLeft: function(event, direction, distance, duration, fingerCount, fingerData) {
        $(this).find('.pagination__next').click();
      },
      swipeRight: function(event, direction, distance, duration, fingerCount, fingerData) {
        $(this).find('.pagination__prev').click();
      },
      // Default is 75px, set to 0 for demo so any distance triggers swipe
      threshold: 140
    });
  });


  // nav toggle
  $('#js-nav-toggle, #js-city-block-wrapper').on('click', function() {
    $('#js-nav-toggle').toggleClass('active');
    $('html').toggleClass('menu-open');
  });

  // when the user hits esc, close the modal
  $document.keydown(function(e) {
    if (e.which == 27) {
      $('#js-nav-toggle').removeClass('active');
      $('html').removeClass('menu-open');
    }
  });

  // color pattern
  $('#js-color-pattern').find('span').on('mouseenter', function() {
    var $color = $(this).data('col');

    // remove all colors
    var $intro = $('#js-intro-section');

    $intro.removeClass (function (index, className) {
      return (className.match (/\bcolor-\S+/g) || []).join(' ');
    });

    switch ($color) {
      case '#5ac8fa':
        $color = 'light-blue';
        break;
      case '#fee94e':
        $color = 'yellow';
        break;
      case '#fb9638':
        $color = 'orange';
        break;
      case '#cdd855':
        $color = 'olive';
        break;
      case '#53d76a':
        $color = 'green';
        break;
      case '#ff554d':
        $color = 'red';
        break;
      case '#fd82c4':
        $color = 'pink';
        break;
      case '#7b7c84':
        $color = 'dark-grey';
        break;
      case '#fff':
        $color = 'white';
        break;
      case '#414cb7':
        $color = 'purple';
        break;
      default:
    }

    // add this color
    $intro.addClass('color-' + $color);
  });
});

$window.load(function() {
  // header animations
  setTimeout(function() {
    $('#js-header-animation1').addClass('fade-in-up');
  }, 500)
  setTimeout(function() {
    $('#js-header-animation2').addClass('fade-in-up');
  }, 800)
  setTimeout(function() {
    $('#js-header-animation3').addClass('fade-in-up');
  }, 1100)
  setTimeout(function() {
    $('#js-header-animation3').find('span').addClass('strike-through');
  }, 1800)

  // particles fade in
  setTimeout(function() {
    $('#particles-js').css('opacity', '1');
  }, 2400)
});
