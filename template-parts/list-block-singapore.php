<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="austin" class="pagination__prev" data-name="Austin">
        <span class="visuallyhidden">prev</span>
        <div class="city-block">
          <h2 class="city-block__title">austin</h2>
          <img class="city-block__image" src="img/austin.svg" alt="">
        </div>
      </a>
      <a href="manchester" class="pagination__next" data-name="Manchester">
        <span class="visuallyhidden">next</span>
        <div class="city-block">
          <h2 class="city-block__title">manchester</h2>
          <img class="city-block__image" src="img/manchester.svg" alt="">
        </div>
      </a>
    </div>
  </div>

  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">Singapore</h2>
        <img class="city-block__image" src="img/singapore.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-singapore">
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option1">
          <label for="location-singapore-option1"></label>
          <h2 class="list-block__title">Level 33</h2>
          <p class="list-block__description">Located on the 33rd floor of Marina Bay Financial Tower, this urban craft brewery claims to be the highest in Asia. A great place to drink in the sights of Downtown Singapore and Marina Bay, the best time to visit is as the sun goes down to witness the free light show across the Bay.</p>
          <a href="https://goo.gl/maps/MmxJhTL7Fw92" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option2">
          <label for="location-singapore-option2"></label>
          <h2 class="list-block__title">East Coast Park</h2>
          <p class="list-block__description">This narrow, five mile long park has something for everyone. Hire bikes, rollerblades or dinghies to get around, take a picnic and rent a BBQ pit or just sit by the beach and watch the ships go by.</p>
          <a href="https://goo.gl/maps/mBypz3jzX992" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option3">
          <label for="location-singapore-option3"></label>
          <h2 class="list-block__title">Henderson Waves / Southern Ridges Canopy Walk</h2>
          <p class="list-block__description">Despite being in the middle of a busy metropolis you’re never really too far from the jungle in Singapore and this walk shows off the best of both. The Henderson Waves is an Instagram worthy bridge with great views over the West Coast. The bridge is a halfway point of a longer walk where you may well share the path with the local monkeys and maybe even a wild pig or two.</p>
          <a href="https://goo.gl/maps/usZzK2KHDJp" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option4">
          <label for="location-singapore-option4"></label>
          <h2 class="list-block__title">Red Star Restaurant</h2>
          <p class="list-block__description">The ‘Aunties’ at this  Singaporean institution have been serving dim sum from carts for the best part of 50 years now and one look at the décor of this place will immediately transport you back to the 70s. You’ll probably find better tasting dim sum elsewhere but the atmosphere is worth making the trip for, especially on a weekend.</p>
          <a href="https://goo.gl/maps/ERos5XRmozN2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option5">
          <label for="location-singapore-option5"></label>
          <h2 class="list-block__title">28 Hong Kong Street</h2>
          <p class="list-block__description">Consistently voted in the Top 50 Bars in the world for the past five years and rightly so, this speakeasy style cocktail bar is a total go-to when nothing but a Manhattan will do.</p>
          <a href="https://goo.gl/maps/MHUJz23aWpA2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option6">
          <label for="location-singapore-option6"></label>
          <h2 class="list-block__title">People’s Park Complex</h2>
          <p class="list-block__description">Singaporeans love to Eat. And Shop. Join them on the ground floor hawker market for a real taste of Singapore’s multicultural food heritage, where you can get everything from Indian Roti to Szechuan style hot-pot. Then head upstairs for a wander around the tailors and fabric sellers before finishing with a session of reflexology at Mr Lim’s.</p>
          <a href="https://goo.gl/maps/MHUJz23aWpA2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option7">
          <label for="location-singapore-option7"></label>
          <h2 class="list-block__title">Thian Hock Keng Temple</h2>
          <p class="list-block__description">One of the oldest Chinese Hokkien Temples in Singapore where travellers would offer prayers to Mazu, The Goddess of the Sea. A little downtown oasis of calm and cool with an interesting story to tell about the role of Chinese immigrants in Singapore.</p>
          <a href="https://goo.gl/maps/PboDsRMjUsQ2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option8">
          <label for="location-singapore-option8"></label>
          <h2 class="list-block__title">Employee’s Only Singapore</h2>
          <p class="list-block__description">The Singaporean outpost of this legendary New York Cocktail joint has imported all the 30’s style (and even the head bartender!) of it’s parent bar. They do a mean Steak Tartare and the drinks and service are everything you’d expect too.</p>
          <a href="https://goo.gl/maps/mJsgBwvbw8k" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option9">
          <label for="location-singapore-option9"></label>
          <h2 class="list-block__title">Momma Kong’s</h2>
          <p class="list-block__description">Crab Crab and more Crab. Roll up your sleeves and get stuck in to Singapore’s national dish. This place does two of the best versions of Singapore Chilli Crab, so decide between the Red Chilli or Black Pepper and don’t forget the fried mantou (bread) buns to sop up all that juice!</p>
          <a href="https://goo.gl/maps/GGDH1D9tnxt" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-singapore-option10">
          <label for="location-singapore-option10"></label>
          <h2 class="list-block__title">Haw Par Villa</h2>
          <p class="list-block__description">This place is just plain freaky! Billed as “the world’s only eclectic Chinese mythological park of its kind”, it was built to represent the 10 Courts of Hell in Chinese folklore – because, why not?</p>
          <a href="https://goo.gl/maps/WXN7uRNrnWH2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
      </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/IMAGEHERE.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by BULLY</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--singapore" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's Singapore done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
