<div class="choose-a-city-block">
  <div class="container">
    <ul class="choose-a-city-block__slider" id="js-city-slider">
      <li class="choose-a-city-block__slide">
        <div class="city-block">
          <a href="manchester">
            <h2 class="city-block__title">manchester</h2>
            <img class="city-block__image" src="img/manchester.svg" alt="">
          </a>
        </div>
      </li>
      <li class="choose-a-city-block__slide">
        <div class="city-block">
          <a href="new-york">
            <h2 class="city-block__title">new york</h2>
            <img class="city-block__image" src="img/new-york.svg" alt="">
          </a>
        </div>
      </li>
      <li class="choose-a-city-block__slide">
        <div class="city-block">
          <a href="glasgow">
            <h2 class="city-block__title">glasgow</h2>
            <img class="city-block__image" src="img/glasgow.svg" alt="">
          </a>
        </div>
      </li>
      <li class="choose-a-city-block__slide">
        <div class="city-block">
          <a href="berlin">
            <h2 class="city-block__title">berlin</h2>
            <img class="city-block__image" src="img/berlin.svg" alt="">
          </a>
        </div>
      </li>
      <li class="choose-a-city-block__slide">
        <div class="city-block">
          <a href="austin">
            <h2 class="city-block__title">austin</h2>
            <img class="city-block__image" src="img/austin.svg" alt="">
          </a>
        </div>
      </li>
    </ul>
  </div>
</div>
