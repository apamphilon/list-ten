<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="berlin" class="pagination__prev" data-name="Berlin">
        <span class="visuallyhidden">prev</span>
        <div class="city-block">
          <h2 class="city-block__title">berlin</h2>
          <img class="city-block__image" src="img/berlin.svg" alt="">
        </div>
      </a>
      <a href="singapore" class="pagination__next" data-name="Singapore">
        <span class="visuallyhidden">next</span>
        <div class="city-block">
          <h2 class="city-block__title">singapore</h2>
          <img class="city-block__image" src="img/singapore.svg" alt="">
        </div>
      </a>
    </div>
  </div>

  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">Austin</h2>
        <img class="city-block__image" src="img/austin.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-berlin">
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option1">
          <label for="location-austin-option1"></label>
          <h2 class="list-block__title">Micklethwaits Craft Meats</h2>
          <p class="list-block__description">Skip the lines at the more famous Franklin’s BBQ and head a little further down the road to find this hidden BBQ truck gem. Try the Brisket Frito Pie and have your mind blown.</p>
          <a href="https://goo.gl/maps/QisXeUqpFuA2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option2">
          <label for="location-austin-option2"></label>
          <h2 class="list-block__title">Vera Cruz All Natural Taco</h2>
          <p class="list-block__description">THE best tacos in Austin. Super simple, super fresh and super delicious. It's run by three sisters out of a converted school bus. Get a Migas (breakfast taco) and the Chicken Mole.</p>
          <a href="https://goo.gl/maps/KQqNnSKX1jS2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option3">
          <label for="location-austin-option3"></label>
          <h2 class="list-block__title">Jester King Brewery</h2>
          <p class="list-block__description">Some of the greatest beer currently made in the U.S (and probably the world, too) coupled with stunning hill country scenery. Pick up a couple of bottles to take home, too.</p>
          <a href="https://goo.gl/maps/qsLAa7tdVMR2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option4">
          <label for="location-austin-option4"></label>
          <h2 class="list-block__title">Blue Hole</h2>
          <p class="list-block__description">It's all in the name. Pack a cooler and some towels and enjoy the cool temperatures of the clear, jewel-toned water from May- September. In the off-season you can hike the various dog-friendly trails, just be mindful that the swimming area is separate and doesn't allow dogs.</p>
          <a href="https://goo.gl/maps/Yu79BXrcEC62" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option5">
          <label for="location-austin-option5"></label>
          <h2 class="list-block__title">Zilker Bark</h2>
          <p class="list-block__description">Barton Springs is awesome, but it gets hella crowded in the summer. Take a tiny detour to the left and there’s a dog-friendly area where you can take a picnic and swim in the perpetually refreshing river.</p>
          <a href="https://goo.gl/maps/suf22CrYe2w" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option6">
          <label for="location-austin-option6"></label>
          <h2 class="list-block__title">Weather Up</h2>
          <p class="list-block__description">The best cocktails in Austin served up in an awesome outdoor area. They even host drive-in movies some nights and the bar snack-y food is top notch. A distinct lack of pretentiousness sets this place apart from many of its more image-conscious peers.</p>
          <a href="https://goo.gl/maps/NdUcWoaeHXH2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option7">
          <label for="location-austin-option7"></label>
          <h2 class="list-block__title">Hamilton Pool</h2>
          <p class="list-block__description">A beautiful natural pool and grotto that's worth a day trip away from the city. It's not pet-friendly and you have to make a reservation to enter, but it's totally worth it.</p>
          <a href="https://goo.gl/maps/6AjrwNrxUEz" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option8">
          <label for="location-austin-option8"></label>
          <h2 class="list-block__title">Carousel Lounge</h2>
          <p class="list-block__description">A relic of ‘old Austin,’ tucked away up north is this gem of a music venue. No cover charge, cheap drinks, live (and extremely eclectic) music every night. Oh, and it’s BYOL. That's not a typo, folks. Bring your own liquor and they’ll provide the mixers. Just make sure you have one of several ride-share apps downloaded to your phone for the journey home.</p>
          <a href="https://goo.gl/maps/EhYReYx8D9z" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option9">
          <label for="location-austin-option9"></label>
          <h2 class="list-block__title">San Marcos River</h2>
          <p class="list-block__description">A nice 45-minute drive south of Austin is the college town of San Marcos. Don’t let the fear of streets overrun with undergrads dissuade you, they have the best tubing river. Head to the Lion’s Club, rent a tube with cup holders and take plenty of beers.</p>
          <a href="https://goo.gl/maps/m9AUKe1f7KF2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-austin-option10">
          <label for="location-austin-option10"></label>
          <h2 class="list-block__title">Flat Track Coffee</h2>
          <p class="list-block__description">Housed on East Cesar Chavez, this is arguably the best coffee in Austin. The menu is limited but they stand by the quality of their coffee and don't need any frills. They share the space with a Bike rental/repair shop too.</p>
          <a href="https://goo.gl/maps/yW6Q1c7hWwo" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
      </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/tom-austin.jpg" alt="" class="list-block__meta-image">
        <img src="img/avatars/dist/andrea-austin.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by Tom and Anthea</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--austin" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's Austin done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
