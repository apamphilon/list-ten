<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="singapore" class="pagination__prev" data-name="Singapore">
        <span class="visuallyhidden">prev</span>
        <div class="city-block city-block-new-york">
          <h2 class="city-block__title">singapore</h2>
          <img class="city-block__image" src="img/singapore.svg" alt="">
        </div>
      </a>
      <a href="new-york" class="pagination__next" data-name="New York">
        <span class="visuallyhidden">next</span>
        <div class="city-block city-block-new-york">
          <h2 class="city-block__title">New York</h2>
          <img class="city-block__image" src="img/new-york.svg" alt="">
        </div>
      </a>
    </div>
  </div>
  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">Manchester</h2>
        <img class="city-block__image" src="img/manchester.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-manchester">
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option1">
          <label for="location-manchester-option1"></label>
          <h2 class="list-block__title">Common</h2>
          <p class="list-block__description">Hands down, my favourite bar on the planet. Always has been, always will be. Good for food, beer and wibes.</p>
          <a href="https://goo.gl/maps/mNFAvGTdpDu" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option2">
          <label for="location-manchester-option2"></label>
          <h2 class="list-block__title">The Castle</h2>
          <p class="list-block__description">Hands down, my favourite pub on the planet. Also good for wibes, and a solid jukebox and small gig venue out the back. </p>
          <a href="https://goo.gl/maps/VRUE8asumH32" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option3">
          <label for="location-manchester-option3"></label>
          <h2 class="list-block__title">Rudy’s Pizza</h2>
          <p class="list-block__description">Still pretty new and, deservedly very busy, but so worth it – proper neopolitan pizza in this laid back neighbourhood hangout.</p>
          <a href="https://goo.gl/maps/8HTSj46jPhC2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option4">
          <label for="location-manchester-option4"></label>
          <h2 class="list-block__title">Fred Aldous</h2>
          <p class="list-block__description">If Willy Wonka was in to crafting instead of chocolate, his name would be Fred Aldous.</p>
          <a href="https://goo.gl/maps/QrDhtLFpksL2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option5">
          <label for="location-manchester-option5"></label>
          <h2 class="list-block__title">Hidden</h2>
          <p class="list-block__description">New club. Instantly iconic.</p>
          <a href="https://goo.gl/maps/kyQSusgCg6A2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option6">
          <label for="location-manchester-option6"></label>
          <h2 class="list-block__title">Football</h2>
          <p class="list-block__description">Manchester Utd. Manchester City. Manchester City Womens. Take your pick, but go watch something.</p>
          <a href="https://goo.gl/maps/e4ZwRTbtu3q" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option7">
          <label for="location-manchester-option7"></label>
          <h2 class="list-block__title">Yadgers</h2>
          <p class="list-block__description">Curry for lunch might not be for everyone, but it’s an institution in Manchester. And this is the best (and spiciest). Go in and ask for “rice and three”.</p>
          <a href="https://goo.gl/maps/JqAd7GYMiXN2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option8">
          <label for="location-manchester-option8"></label>
          <h2 class="list-block__title">Whitworth Gallery</h2>
          <p class="list-block__description">They absolutely nailed the renovation in 2015, making this old “gallery in the park” the perfect place to spend an afternoon.</p>
          <a href="https://goo.gl/maps/64JaoYF37dR2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option9">
          <label for="location-manchester-option9"></label>
          <h2 class="list-block__title">Bellevue</h2>
          <p class="list-block__description">Dog racing. Get your bet on.</p>
          <a href="https://goo.gl/maps/GW2FtSRomrM2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-manchester-option10">
          <label for="location-manchester-option10"></label>
          <h2 class="list-block__title">The Temple</h2>
          <p class="list-block__description">Drink in the best renovated public toilet in town. They know their shit (and music).</p>
          <a href="https://goo.gl/maps/b8uWMwWiwjm" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
      </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/ruthersish.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by Ruthersish</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--manchester" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's Manchester done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
