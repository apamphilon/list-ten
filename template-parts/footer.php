    <div class="footer-about" id="js-about">
      <div class="container">
        <p>All the lists here are written by friends who live in each of the cities featured. What they recommend is totally up to them… just what they think friends (you!) should see to get the most out of a visit. Not necessarily the “BIG” things, but the best things to get a feel for a place.</p>

        <p>If you’d like to write a list, or have any ideas/feedback – give us a shout on <a href="https://twitter.com/L10travel" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>.</p>
      </div>
    </div>

    <footer id="footer" class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-9">
            <p>Lists by Ruthersish, Kyle, Lalley, Martyn, Jay, Mo, Alison, Tom, Andrea, Rosie, Beth and Alex.</p>
            <p>Designed by <a href="http://www.randomfeature.co.uk/" target="_blank">Random Feature</a>. Built by <a href="https://www.apamphilon.com" target="_blank">apamphilon</a>. &copy; <?php echo date('Y'); ?>. <a href="#" id="js-about-trigger">About List Ten.</a></p>
          </div>
          <div class="col-sm-3 pull-right">
            <ul class="social-footer">
              <li class="social-footer__list-item"><a href="https://twitter.com/L10travel" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li class="social-footer__list-item"><a href="https://www.facebook.com/L10travel" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div><!-- .container -->
    </footer>

    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
    <script src="js/main.min.js"></script>

    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107267852-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());

      gtag('config', 'UA-107267852-1');
    </script>
  </body>
</html>
