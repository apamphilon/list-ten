<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="glasgow" class="pagination__prev" data-name="Glasgow">
        <span class="visuallyhidden">prev</span>
        <div class="city-block">
          <h2 class="city-block__title">glasgow</h2>
          <img class="city-block__image" src="img/glasgow.svg" alt="">
        </div>
      </a>
      <a href="austin" class="pagination__next" data-name="Austin">
        <span class="visuallyhidden">next</span>
        <div class="city-block">
          <h2 class="city-block__title">austin</h2>
          <img class="city-block__image" src="img/austin.svg" alt="">
        </div>
      </a>
    </div>
  </div>

  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">Berlin</h2>
        <img class="city-block__image" src="img/berlin.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-berlin">
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option1">
          <label for="location-berlin-option1"></label>
          <h2 class="list-block__title">Teufelsberg</h2>
          <p class="list-block__description">Post-apocalyptic-style former USA listening station, built on the rubble of a Nazi military college. Sneak through the fence and pay security to take you on a tour.</p>
          <a href="https://www.google.co.uk/maps/place/Teufelsberg/@52.4972222,13.2389224,17z/data=!3m1!4b1!4m5!3m4!1s0x47a857677b59223f:0x3f9fca18262304ed!8m2!3d52.4972222!4d13.2411111" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option2">
          <label for="location-berlin-option2"></label>
          <h2 class="list-block__title">Cycle in the parks</h2>
          <p class="list-block__description">Tempelhof is the old Nazi airport, Treptow has the imposing Russian war memorial and Hasenheide has a camel inhaling the fumes from all the weed dealers.</p>
          <a href="https://goo.gl/maps/wXGvTfBXkHk" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option3">
          <label for="location-berlin-option3"></label>
          <h2 class="list-block__title">Eat in Adana, Kreuzberg</h2>
          <p class="list-block__description">Turn up, register your interest and hope they give you the best seat around the coals where all the meat is salted and fired.</p>
          <a href="https://www.google.co.uk/maps/place/Adana+Grill+House+Restaurant/@52.4996351,13.4246656,17z/data=!3m1!4b1!4m5!3m4!1s0x47a84e357faa2cad:0x3624d3483f38be43!8m2!3d52.4996351!4d13.4268543" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option4">
          <label for="location-berlin-option4"></label>
          <h2 class="list-block__title">Lenau Stuben</h2>
          <p class="list-block__description">Among Neukolln and Kreuzberg’s bars lie Kneipe – old man pubs. Drink local beer, admire the tree that grows through this one, experience true Berliner Schnauze.</p>
          <a href="https://goo.gl/maps/xsqMGbXxVsw" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option5">
          <label for="location-berlin-option5"></label>
          <h2 class="list-block__title">Stasi Headquarters</h2>
          <p class="list-block__description">See the site of Berlin’s most significant post-partition demos, and see the massive extent to which the “Ministry for State Security” had gotten out of hand.</p>
          <a href="https://goo.gl/maps/uq5DpmpXEoN2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option6">
          <label for="location-berlin-option6"></label>
          <h2 class="list-block__title">A day at the lake</h2>
          <p class="list-block__description">Spend the day at one of the lakes which surround the capital. There are 80, but for a buzz, clear water and beautiful forests, you can’t do much better than these two.</p>
          <a href="https://goo.gl/maps/Lo4xwVzJyCD2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option7">
          <label for="location-berlin-option7"></label>
          <h2 class="list-block__title">Clubbing in Tresor</h2>
          <p class="list-block__description">Descend into the depths on a Friday or Saturday, feel your skin prickle as temperature drops and the techno gets louder.</p>
          <a href="https://goo.gl/maps/GkfGQPqYyg42" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option8">
          <label for="location-berlin-option8"></label>
          <h2 class="list-block__title">Watch FC Union</h2>
          <p class="list-block__description">Formed in 1906, the Union’s weathered Nazis, a Cold War, and a failing GDR. Fans rebuilt the stadium in 2008. Expect passion, pride and an epic Friday night.</p>
          <a href="https://goo.gl/maps/V28emcwKkbT2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option9">
          <label for="location-berlin-option9"></label>
          <h2 class="list-block__title">Tante Lisbeth</h2>
          <p class="list-block__description">Kegelbahn is the german version of ten-pin bowling… with nine 9 pins. Tante Lisbeth is one of a few Berlin bars which feature a Kegelbahn bowling alley - the 80’s decor, cosy den atmosphere and CD player makes this one of the best.</p>
          <a href="https://goo.gl/maps/1Muf2AeWg9x" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-berlin-option10">
          <label for="location-berlin-option10"></label>
          <h2 class="list-block__title">Mauerpark Flohmarkt</h2>
          <p class="list-block__description">The main attraction is the flea market which has clothes, furniture, records, bikes and a lot more. Running every sunday, the park alongside it is like a mini music festival with bands and performers every few metres. To top it off, there is a legendary outdoor karaoke session which has been running since 2009.</p>
          <a href="https://goo.gl/maps/QBkAdBTT1Bz" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
      </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/lnz-berlin.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by LNZ</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--berlin" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's Berlin done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
