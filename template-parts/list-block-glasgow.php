<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="new-york" class="pagination__prev" data-name="New York">
        <span class="visuallyhidden">prev</span>
        <div class="city-block">
          <h2 class="city-block__title">new york</h2>
          <img class="city-block__image" src="img/new-york.svg" alt="">
        </div>
      </a>
      <a href="berlin" class="pagination__next" data-name="Berlin">
        <span class="visuallyhidden">next</span>
        <div class="city-block">
          <h2 class="city-block__title">berlin</h2>
          <img class="city-block__image" src="img/berlin.svg" alt="">
        </div>
      </a>
    </div>
  </div>

  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">Glasgow</h2>
        <img class="city-block__image" src="img/glasgow.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-glasgow">
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option1">
        <label for="location-glasgow-option1"></label>
        <h2 class="list-block__title">Sharmanka</h2>
        <p class="list-block__description">Awesome kinetic theatre housed in Trongate arts centre with a Russian restaurant in it (sticking with the sharmanka/communist theme!) and various exhibition spaces.</p>
        <a href="https://goo.gl/maps/tvi2p3wNuD22" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option2">
        <label for="location-glasgow-option2"></label>
        <h2 class="list-block__title">Modern Institute</h2>
        <p class="list-block__description">Just round the corner from Sharmanka, the Modern Institute which tends to have some far out installations, but the space itself is worth checking out.</p>
        <a href="https://goo.gl/maps/bbAhkqLVN8G2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option3">
        <label for="location-glasgow-option3"></label>
        <h2 class="list-block__title">Mono</h2>
        <p class="list-block__description">You won't have to walk far, because close-by is Mono for some tasty vegan (surprisingly hearty!) grub and a peruse through some vinyl.</p>
        <a href="https://goo.gl/maps/X9kUuhfgjVS2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option4">
        <label for="location-glasgow-option4"></label>
        <h2 class="list-block__title">Panopticon</h2>
        <p class="list-block__description">The world's oldest surviving music hall which is like going back in time (Laurel and Hardy played there!). A sweet and shady part of town.</p>
        <a href="https://goo.gl/maps/41HQJNL5jRs" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option5">
        <label for="location-glasgow-option5"></label>
        <h2 class="list-block__title">13th Note</h2>
        <p class="list-block__description">Another one nearby is the 13th Note, which is decent for food, drink, music.</p>
        <a href="https://goo.gl/maps/uW67Jm73mr62" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option6">
        <label for="location-glasgow-option6"></label>
        <h2 class="list-block__title">Gallery of Modern Art</h2>
        <p class="list-block__description">If you're into that kind of thing, sometimes great, sometimes bit pretentious.</p>
        <a href="https://goo.gl/maps/4XYDviEDsrM2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option7">
        <label for="location-glasgow-option7"></label>
        <h2 class="list-block__title">Tramway</h2>
        <p class="list-block__description">A cool arts space in the South Side with nice gardens.</p>
        <a href="https://goo.gl/maps/maGkkQr2aYS2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option8">
        <label for="location-glasgow-option8"></label>
        <h2 class="list-block__title">Botanic Gardens</h2>
        <p class="list-block__description">You should definitely go to the Botanic Gardens and walk along the river to Kelvingrove park and to Kelvingrove museum, maybe up to the University. Its well worth a wander if you have nice enough weather. </p>
        <a href="https://goo.gl/maps/HYntA9J1xKP2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option9">
        <label for="location-glasgow-option9"></label>
        <h2 class="list-block__title">SubClub</h2>
        <p class="list-block__description">Of course. The world-famous nightclub.</p>
        <a href="https://goo.gl/maps/XfFPe3opGNH2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
      <li class="list-block__list-item">
        <input type="checkbox" id="location-glasgow-option10">
        <label for="location-glasgow-option10"></label>
        <h2 class="list-block__title">Open top tour bus</h2>
        <p class="list-block__description">Good way to see the whole city quite quickly and then you can decide what things you want to investigate more.  That includes the Barras and the Necropolis which are both worth a look but maybe not top of my list if you only have one weekend.</p>
        <a href="https://goo.gl/maps/WHeHvWVYFQ22" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
      </li>
    </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/rosie-glasgow.jpg" alt="" class="list-block__meta-image">
        <img src="img/avatars/dist/beth-glasgow.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by Rosie and Beth</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--glasgow" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's Glasgow done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
