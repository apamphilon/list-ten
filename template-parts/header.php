<header id="header" class="site-header" role="banner">
  <div class="container">
    <div class="nav-toggle-outer" id="js-nav-toggle">
      <a class="nav-toggle">
        <div class="bars">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <div class="other-bar"></div>
        </div>
      </a>
    </div>

    <h1 class="site-logo"><a href="/">List Ten</a></h1>
  </div>
</header>
