<div class="about-block section-padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-1">
        <div class="phone-block">
          <h2 class="phone-block__title">List Ten is a simple travel idea...</h2>
          <p class="phone-block__copy">Don’t get overwhelmed with infinite reviews and recommendations. Check off the ten things on our list and you’ll go home happy.</p>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="phone-vid">
          <img src="img/device.svg" alt="svg mobile phone" class="phone-block__image">
          <video loop muted playsinline class="phone-vid__video" poster="img/video-fallback.png">
            <source src="videos/anim-home.mov" type="video/mp4">
            <img src="img/video-fallback.png" title="Your browser does not support the <video> tag" />
          </video>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-7 col-sm-offset-1">
        <div class="quote">
          <span class="quote__quote">The perfect antidote to my travel FOMO!</span>
          <span class="quote__person">Alison Taylor, serial traveler.</span>
        </div>
      </div>
    </div>
  </div>
</div>
