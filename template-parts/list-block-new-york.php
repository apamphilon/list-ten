<div class="list-block section-padding" id="js-list-block">
  <div id="js-pagination-wrapper">
    <div class="pagination" id="js-pagination">
      <a href="manchester" class="pagination__prev" data-name="Manchester">
        <span class="visuallyhidden">prev</span>
        <div class="city-block">
          <h2 class="city-block__title">manchester</h2>
          <img class="city-block__image" src="img/manchester.svg" alt="">
        </div>
      </a>
      <a href="glasgow" class="pagination__next" data-name="Glasgow">
        <span class="visuallyhidden">next</span>
        <div class="city-block">
          <h2 class="city-block__title">glasgow</h2>
          <img class="city-block__image" src="img/glasgow.svg" alt="">
        </div>
      </a>
    </div>
  </div>

  <div class="container">
    <div id="js-city-block-wrapper">
      <div class="city-block" id="js-city-block">
        <h2 class="city-block__title">New York</h2>
        <img class="city-block__image" src="img/new-york.svg" alt="">
      </div>
    </div>

    <div id="js-list-block-wrapper">
      <ul class="list-block__list" id="js-list-location-new-york">
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option1">
          <label for="location-new-york-option1"></label>
          <h2 class="list-block__title">Momofuku</h2>
          <p class="list-block__description">David Chang's big five will make love to every tastebud you possess. Ssam for meat. Milk Bar for sweet. Ma Peche dim sum. Ko for fancy. Noodle Bar... well, you can guess.</p>
          <a href="https://goo.gl/maps/WdWtkatSJ9k" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option2">
          <label for="location-new-york-option2"></label>
          <h2 class="list-block__title">Shopsin's</h2>
          <p class="list-block__description">A tiny, tucked away, diner/restaurant created by an eccentric chef. Follow their rules. But also put both the hot sauce and syrup on your mac and cheese pancakes.</p>
          <a href="https://goo.gl/maps/pwsaGuukPy22" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option3">
          <label for="location-new-york-option3"></label>
          <h2 class="list-block__title">Bowery Ballroom</h2>
          <p class="list-block__description">Hard to find the right adjective for this place. Bigsmall. Intimassive. Freshistoric. A big fat slice of the Manhattan vibe and like no other music venue.</p>
          <a href="https://goo.gl/maps/FY1r4yTmjyB2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option4">
          <label for="location-new-york-option4"></label>
          <h2 class="list-block__title">Smorgasburg</h2>
          <p class="list-block__description">A giant flea market with delicious food that you can eat on the spot, and treats to save for later. Be sure to wear loose-fitting pants.</p>
          <a href="https://goo.gl/maps/mWHJXzFsgmN2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option5">
          <label for="location-new-york-option5"></label>
          <h2 class="list-block__title">Rough Trade</h2>
          <p class="list-block__description">An extension of the original London mainstays. Vinyl, cd's, books or magazines - this record shop has it all including a 250 capacity venue in the back of the house.</p>
          <a href="https://goo.gl/maps/qKu5T2csW6z" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option6">
          <label for="location-new-york-option6"></label>
          <h2 class="list-block__title">The Metropolitan Museum of Art</h2>
          <p class="list-block__description">Art lover or avoider, the MET has something for you. And if it doesn't, you weren’t looking hard enough and need to go back.</p>
          <a href="https://www.google.co.uk/maps/place/The+Metropolitan+Museum+of+Art/@40.7794366,-73.9654327,17z/data=!3m2!4b1!5s0x89c25896e219e489:0x6c1168e3cbcffb86!4m5!3m4!1s0x89c25896f660c26f:0x3b2fa4f4b6c6a1fa!8m2!3d40.7794366!4d-73.963244" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option7">
          <label for="location-new-york-option7"></label>
          <h2 class="list-block__title">Central Park</h2>
          <p class="list-block__description">Famous for a reason, this escape from the madness is only ever a train ride away. Take in a show, see Shakespeare in the Park, touch the grass and sit under a tree.</p>
          <a href="https://goo.gl/maps/pWsGUReBfMs" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option8">
          <label for="location-new-york-option8"></label>
          <h2 class="list-block__title">Angel's Share</h2>
          <p class="list-block__description">Secreted behind a Japanese street food restaurant is a cocktail bar. Enter with an open mind. Drink gratefully from whatever they choose to give you.</p>
          <a href="https://goo.gl/maps/rRtnnHmTH3S2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option9">
          <label for="location-new-york-option9"></label>
          <h2 class="list-block__title">Dog Run in Tompkin’s Square Park</h2>
          <p class="list-block__description">Get a coffee and wander up there. We hang and name the dogs. Also, outdoor ping pong, but you have to bring your own paddle.</p>
          <a href="https://goo.gl/maps/kduNxNiRYQE2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
        <li class="list-block__list-item">
          <input type="checkbox" id="location-new-york-option10">
          <label for="location-new-york-option10"></label>
          <h2 class="list-block__title">Four Horsemen</h2>
          <p class="list-block__description">James Murphy’s newish restaurant endeavor highlighting biodynamic wines and small plates. The beef tartare is the best thing that I have eaten recently and tastes like an expensive, raw In n Out cheeseburger.</p>
          <a href="https://goo.gl/maps/qFMbRDamZLS2" target="_blank" class="list-block__map-marker"><span class="visuallyhidden">map</span></a>
        </li>
      </ul>
    </div>

    <div id="js-list-meta-wrapper">
      <div class="list-block__meta" id="js-list-meta">
        <img src="img/avatars/dist/jay-nyc.jpg" alt="" class="list-block__meta-image">
        <img src="img/avatars/dist/maureen-nyc.jpg" alt="" class="list-block__meta-image">
        <h3 class="list-block__meta-title">List by Jay and Mo</h3>
      </div>
    </div>
  </div>
</div>

<div id="js-list-complete-wrapper">
  <div class="modal list-complete list-complete--new-york" id="js-list-complete">
    <div class="modal-content">
      <div class="container">
        <h2 class="list-complete__title">List Ten Complete!</h2>
        <p class="list-complete__copy">Well, that's New York done. You can go home happy!</p>
        <span class="btn close" href="/">Back to list</span>
      </div>
    </div>
  </div>
</div>
