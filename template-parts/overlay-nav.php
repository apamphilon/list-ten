<div class="overlay-nav" id="js-overlay-nav">
  <div class="table">
    <div class="table-cell">
      <nav class="main-navigation">
        <div class="container">
          <ul>
            <li><a href="manchester">manchester</a></li>
            <li><a href="new-york">new york</a></li>
            <li><a href="glasgow">glasgow</a></li>
            <li><a href="berlin">berlin</a></li>
            <li><a href="austin">austin</a></li>
            <li><a href="singapore">singapore</a></li>
          </ul>
        </div><!-- .container -->
      </nav>
    </div>
  </div>
</div>
